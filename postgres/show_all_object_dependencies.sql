SELECT
d.classid,
d.objid,
d.objsubid,
d.refclassid,
d.refobjid,
d.refobjsubid,
d.deptype,
c1.relname AS obj_relname,
c2.relname AS ref_relname
--t.*,
--r.*
FROM pg_depend d
INNER JOIN pg_class c1 ON (c1.oid = d.classid)
INNER JOIN pg_class c2 ON (c2.oid = d.refclassid)
--LEFT JOIN pg_type t ON (t.oid = d.objid)
--LEFT JOIN pg_rewrite r ON (r.oid = d.objid)
WHERE d.refobjid IN ('<mytable/myview>'::regclass, '<mytable/myview>'::regtype)
ORDER BY d.refobjid, d.classid;
