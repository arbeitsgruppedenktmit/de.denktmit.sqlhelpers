CREATE TABLE configuration_versions
(
    config_component VARCHAR NOT NULL
        CONSTRAINT configuration_versions_pk
            PRIMARY KEY
        CONSTRAINT cns_configuration_versions_config_components
            CHECK (config_component IN ('data_endpoint', 'data_endpoint_policy', 'data_endpoint_user', 'data_service', 'endpoint_definition', 'endpoint_definition_method', 'settings')),
    config_version BIGINT NOT NULL
        CONSTRAINT cns_configuration_versions_config_version
            CHECK (config_version > 0)
);
COMMENT ON CONSTRAINT cns_configuration_versions_config_components ON configuration_versions IS 'Enforces only a limited list of keys can be used';
COMMENT ON CONSTRAINT cns_configuration_versions_config_version ON configuration_versions IS 'Versions the state of the configurations component. Always incremented on write change.';


CREATE OR REPLACE FUNCTION increment_config_version()
    RETURNS TRIGGER AS
$BODY$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM configuration_versions WHERE config_component = TG_TABLE_NAME) THEN
        RAISE EXCEPTION 'Table % is not under version control', TG_TABLE_NAME
            USING HINT = 'Verify, that an existing configuration_component is equal to the trigger table names';
    END IF;
    UPDATE configuration_versions SET config_version = config_version + 1 WHERE config_component = TG_TABLE_NAME;
    RETURN NEW;
END;
$BODY$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION get_allowed_check_constraint_values(var_constraint_name text)
    RETURNS VARCHAR[] AS
$BODY$
DECLARE
    var_constraint_sql TEXT;
BEGIN
    SELECT check_clause INTO var_constraint_sql FROM information_schema.check_constraints cc WHERE cc.constraint_name = var_constraint_name;
    RETURN (SELECT replace(regexp_replace(var_constraint_sql, '.*ARRAY\[(.*)\]\)::text\[\]\)+\)\)', '{\1}'), '::character varying', ''));
END
$BODY$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE PROCEDURE add_configuration_versioning_triggers(var_constraint_name text)
    LANGUAGE 'plpgsql' AS
$BODY$
DECLARE
    var_table_names VARCHAR[] := get_allowed_check_constraint_values(var_constraint_name);
    var_table_name VARCHAR;
    var_trigger_name VARCHAR;
    var_table_exists BOOLEAN;
    var_needs_insertion BOOLEAN;
BEGIN
    FOREACH var_table_name IN ARRAY var_table_names
        LOOP
            SELECT replace(var_table_name, '''', '') INTO var_table_name;
            SELECT 'track_vchange_' || var_table_name INTO var_trigger_name;
            SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = var_table_name) INTO var_table_exists;
            IF NOT var_table_exists THEN
                RAISE EXCEPTION 'Table ''%'' does not exist.', var_table_name;
            END IF;
            SELECT NOT EXISTS (SELECT 1 FROM configuration_versions WHERE config_component = var_table_name) INTO var_needs_insertion;
            IF var_needs_insertion THEN
                EXECUTE format('INSERT INTO configuration_versions (config_component, config_version) VALUES (''%s'', 1);', var_table_name);
            END IF;
            EXECUTE format('DROP TRIGGER IF EXISTS %s ON %s;', var_trigger_name, var_table_name);
            EXECUTE format('CREATE TRIGGER %s AFTER INSERT OR UPDATE OR DELETE ON %s EXECUTE PROCEDURE increment_config_version();', var_trigger_name, var_table_name);
        END LOOP;
    RETURN;
END;
$BODY$;

CALL add_configuration_versioning_triggers('cns_configuration_versions_config_components');
