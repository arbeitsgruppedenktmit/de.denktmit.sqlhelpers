-- Hibernate default sequence is 50 (seq nextval * 50)
-- Sequence naming should be: `table_name`_`pk` (eg: users_user_id)
-- TBD: more doku needed ;)

select 'SELECT setval('''||c.relname||''', (SELECT floor(MAX('||st.pk||')/50, true) FROM '||st.table_name||')::integer);' from (
    SELECT
        tc.table_name as table_name,
        string_agg(kcu.column_name, '_') as pk,
        CONCAT(tc.table_name, '_', string_agg(kcu.column_name, '_')) || '_seq' as seq
    FROM 
        information_schema.table_constraints AS tc 
    JOIN information_schema.key_column_usage AS kcu
        ON tc.constraint_name = kcu.constraint_name
    WHERE 
        constraint_type = 'PRIMARY KEY' 
    GROUP BY 
        tc.table_name) as st
    INNER JOIN pg_class c ON c.relname::text = st.seq::text
    WHERE c.relkind = 'S';



-- not matched sequences

select c.relname
from pg_class c
WHERE c.relkind = 'S' and c.relname not in (

select c.relname from (
    SELECT
        tc.table_name as table_name,
        string_agg(kcu.column_name, '_') as pk,
        CONCAT(tc.table_name, '_', string_agg(kcu.column_name, '_')) || '_seq' as seq
    FROM 
        information_schema.table_constraints AS tc 
    JOIN information_schema.key_column_usage AS kcu
        ON tc.constraint_name = kcu.constraint_name
    WHERE 
        constraint_type = 'PRIMARY KEY' 
    GROUP BY 
        tc.table_name) as st
    INNER JOIN pg_class c ON c.relname::text = st.seq::text
    WHERE c.relkind = 'S')
