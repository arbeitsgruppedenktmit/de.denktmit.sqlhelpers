WITH view_creations AS (
SELECT 
    c.oid,
    'CREATE OR REPLACE VIEW ' || c.relname || ' AS ' || pg_get_viewdef(c.oid) AS statement
   FROM pg_class c
     LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  WHERE n.nspname NOT IN ('pg_catalog', 'information_schema') AND c.relkind = 'v'::"char"

 UNION

SELECT 
    c.oid,
    'CREATE MATERIALIZED VIEW ' || c.relname || ' AS ' || pg_get_viewdef(c.oid) AS statement
   FROM pg_class c
     LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  WHERE n.nspname NOT IN ('pg_catalog', 'information_schema') AND c.relkind = 'm'::"char"
 )
 SELECT 
 statement::text
 FROM view_creations
 ORDER BY oid;
