WITH view_deletions AS (
SELECT 
    c.oid,
    'DROP VIEW ' || c.relname || ';' AS statement
   FROM pg_class c
     LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  WHERE n.nspname NOT IN ('pg_catalog', 'information_schema') AND c.relkind = 'v'::"char"

 UNION

SELECT 
    c.oid,
    'DROP MATERIALIZED VIEW ' || c.relname || ';' AS statement
   FROM pg_class c
     LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
  WHERE n.nspname NOT IN ('pg_catalog', 'information_schema') AND c.relkind = 'm'::"char"
 )
 SELECT 
 statement::text
 FROM view_deletions
 ORDER BY oid DESC;
